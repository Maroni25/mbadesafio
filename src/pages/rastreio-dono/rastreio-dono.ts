import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-rastreio-dono',
  templateUrl: 'rastreio-dono.html',
})
export class RastreioDonoPage {

  constructor(private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {}

  qrCodeAlert() {
    let alert = this.alertCtrl.create({
      title: 'QRCode',
      message: 'Verifique na coleira do animal se ele possui QRCode.',
      buttons: [
        {
          text: 'Voltar',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Ler QRCode',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

}
