import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { ApiConfig } from '../../constantes/api-config/api-config';
import { MensagemUtilProvider } from '../../service/mensagem-util/mensagem-util';
import { CadastroServiceProvider } from '../../service/cadastro-service/cadastro-service';
import { Storage } from '@ionic/storage';
import { DadosUsuarioProvider } from '../../providers/dados-usuario/dados-usuario';
import { StringUtilProvider } from '../../providers/string-util/string-util';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public paginaLogin:boolean = true;
  public usuario:any = {};
  public codigoConfirmacao:string;
  public novaSenha:string;
  public confirmacaoNovaSenha:string;
  public emailRecuperarSenha:string;
  constructor(private storage: Storage, public cadastroService: CadastroServiceProvider, public mensagemUtil:MensagemUtilProvider,public events: Events,public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {}

  mudarPaginaLogin(){
    this.paginaLogin = true;
  }

  mudarPaginaCadastro(){
    this.paginaLogin = false;
  }

  public cadastrar(){
    let loading = this.loadingCtrl.create({
      content: 'Cadastrando...'
    });
    loading.present();
    if(StringUtilProvider.isSenhaValida(this.usuario.senha)){
      if(this.usuario.senha === this.usuario.confirmacaoSenhaCadastro){
        this.cadastroService.cadastrar(this.usuario).subscribe(result => {
          loading.dismiss();
          this.navCtrl.setRoot(LoginPage);
          this.mensagemUtil.mostrarToastSuccess("Usuário criado com Sucesso!");
        }, error => {
          loading.dismiss(),
          this.mensagemUtil.mostrarToastError(error)
        });
      }else{
        loading.dismiss();
        this.mensagemUtil.mostrarToastErrorString("Senha e confirmação de senha diferentes.");
      }
    }else{
      loading.dismiss();
      this.mensagemUtil.mostrarToastErrorString("Senha muito fraca. Senha precisa ter: 8 caracteres e pelo menos 1 número.");
    }
  }

  public mudarSenha(){
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    if(this.novaSenha !== null && this.confirmacaoNovaSenha !== null && this.novaSenha !== undefined && this.confirmacaoNovaSenha !== undefined){
      if(StringUtilProvider.isSenhaValida(this.novaSenha)){
        if(this.novaSenha === this.confirmacaoNovaSenha){
          let usuario = {
            senha: this.novaSenha,
            email: this.emailRecuperarSenha,
            codigoValidador:this.codigoConfirmacao
          }
          this.cadastroService.mudarSenha(usuario).subscribe(res => {
            loading.dismiss();
            this.mensagemUtil.mostrarToastSuccess("Senha alterada com sucesso!");
          }, error => {
            loading.dismiss(),
            this.mensagemUtil.mostrarToastError(error)
          });
        }else{
          loading.dismiss();
          this.mensagemUtil.mostrarToastErrorString("Senha e confirmação de senha devem ser idênticas.");
        }
      }else{
        loading.dismiss();
        this.mensagemUtil.mostrarToastErrorString("Senha muito fraca. Senha precisa ter: 8 caracteres e pelo menos 1 número.");
      }
    }else{
      loading.dismiss();
      this.mensagemUtil.mostrarToastErrorString("Todos os campos são obrigatórios");
    }
  }

  public logar(){
    let loading = this.loadingCtrl.create({
      content: 'Logando...'
    });
    loading.present();
    this.cadastroService.logar(this.usuario).subscribe(res => {
      let dadosUsuarioBase = res.headers.get("dados-usuario");
      DadosUsuarioProvider.preencherDadosProvider(dadosUsuarioBase);
      this.storage.set("Authorization",res.headers.get("Authorization"));
      ApiConfig.tokenAuth = res.headers.get("Authorization");
      loading.dismiss();
      this.events.publish('user:login');
    }, error => {
      loading.dismiss(),
      this.mensagemUtil.mostrarToastError(error)
    });
  }

  ngOnInit(){
    this.events.publish('user:carregarPaginaInicial');
  }

  public continuarSemLogar(){
    let loading = this.loadingCtrl.create({
      content: 'Entrando...'
    });
    this.events.publish('user:entrarSemLogin');
    loading.dismiss();
  }
}
