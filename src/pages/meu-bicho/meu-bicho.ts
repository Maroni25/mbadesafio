import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadastroPetPage } from '../cadastro-pet/cadastro-pet';

@Component({
  selector: 'page-meu-bicho',
  templateUrl: 'meu-bicho.html',
})
export class MeuBichoPage {

  public bichoPerdido:boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  cadastrarPet(){
    this.navCtrl.push(CadastroPetPage);
  }

}
