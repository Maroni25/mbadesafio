import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DadosUsuarioProvider } from '../../providers/dados-usuario/dados-usuario';
import { MensagemUtilProvider } from '../../service/mensagem-util/mensagem-util';
import { ConfiguracaoServiceProvider } from '../../service/configuracao-service/configuracao-service';

@Component({
  selector: 'page-configuracao',
  templateUrl: 'configuracao.html',
})
export class ConfiguracaoPage {

  public alertaPerdidosProximo:boolean = false;
  public alertaNovasMensagens:boolean = false;

  constructor(public configuracaoService:ConfiguracaoServiceProvider,public mensagemUtil:MensagemUtilProvider,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.alertaPerdidosProximo = DadosUsuarioProvider.configuracao.alertaPerdidosProximo;
    this.alertaNovasMensagens = DadosUsuarioProvider.configuracao.alertaNovasMensagens;
  }

  public atualizar(){
    let loading = this.loadingCtrl.create({
      content: 'Atualizando...'
    });
    loading.present();
    let configuracao = {
      id: DadosUsuarioProvider.configuracao.id,
      alertaPerdidosProximo: this.alertaPerdidosProximo,
      alertaNovasMensagens: this.alertaNovasMensagens
    };
    this.configuracaoService.salvar(configuracao).subscribe(result => {
      DadosUsuarioProvider.configuracao.alertaPerdidosProximo = this.alertaPerdidosProximo;
      DadosUsuarioProvider.configuracao.alertaNovasMensagens = this.alertaNovasMensagens;
      loading.dismiss();
      this.mensagemUtil.mostrarToastSuccess("Configuraçoes atualizadas!");
    }, error => {
      loading.dismiss(),
      this.mensagemUtil.mostrarToastError(error)
    });
  }

}
