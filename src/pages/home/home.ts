import { Component, ViewChild  } from '@angular/core';
import { NavController, LoadingController,Platform } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { MensagemUtilProvider } from '../../service/mensagem-util/mensagem-util';
import { PetServiceProvider } from '../../service/pet-service/pet-service';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker
} from "@ionic-native/google-maps";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public estado:string;
  public cidade:string;

  constructor(public plt: Platform, public googleMaps: GoogleMaps, public petService:PetServiceProvider,public loadingCtrl: LoadingController, public mensagemUtil:MensagemUtilProvider,public navCtrl: NavController, private nativeGeocoder: NativeGeocoder,private geolocation: Geolocation) {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.petService.buscarPetsProximos(resp.coords.latitude,resp.coords.longitude).subscribe(res => {
        let coordinates: LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
        let position = {
          target: coordinates,
          zoom: 17
        };        
      }, error => {
      });
    });
  } 

  formatarLocais(local:any){
    JSON.stringify(local[0]);
    if(local[0] !== null && local[0] !== undefined){
      for(let i = 0; i< local.length; i++){
        local[0]['administrativeArea'] = local[0]['administrativeArea'].replace('"','');
        local[0]['subAdministrativeArea'] = local[0]['subAdministrativeArea'].replace('"','');
      }
      this.estado = local[0]['administrativeArea'];
      this.cidade = local[0]['subAdministrativeArea'];
    }
  }
}

