import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AppServiceProvider } from '../../service/app-service/app-service';

@Component({
  selector: 'page-cadastro-pet',
  templateUrl: 'cadastro-pet.html',
})
export class CadastroPetPage {

  public pet:any = {};
  public textoPet:string;
  public pets:any = [];

  constructor(public platform: Platform,private camera: Camera, public actionSheetCtrl: ActionSheetController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    if(this.pet.nome !== null && this.pet.nome !== undefined){
      this.textoPet = "O "+this.pet.nome+" está perdido?";
    }else{
      this.textoPet = "Ele está perdido?";
    }
  }

  public buscarAcoesCamera(){
    let alert = this.alertCtrl.create({
      title: 'Enviar foto',
      message: 'Selecione o local da foto',
      buttons: [
        {
          text: 'Carregar da galeria',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tirar foto',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }

  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      allowEdit: true,
      targetHeight: 100,
      targetWidth: 100
    }
 
    this.camera.getPicture(options)
      .then((imageData) => {
        let pet:any = {};
        pet.imagem = AppServiceProvider.carregarFoto(imageData);
        this.pets.push(pet);        
      }, (error) => {
        console.error(error);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  salvar(){

  }

}
