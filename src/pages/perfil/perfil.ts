import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, ActionSheetController, Platform } from 'ionic-angular';
import { DadosUsuarioProvider } from '../../providers/dados-usuario/dados-usuario';
import { MensagemUtilProvider } from '../../service/mensagem-util/mensagem-util';
import { CadastroServiceProvider } from '../../service/cadastro-service/cadastro-service';
import { StringUtilProvider } from '../../providers/string-util/string-util';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { normalizeURL} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AppServiceProvider } from '../../service/app-service/app-service';

declare var cordova: any;
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public usuario:any = {};
  public senha:string;
  public confirmacaoSenha:string;
  public photo: string = '';

  constructor(private alertCtrl: AlertController, public platform: Platform,private camera: Camera, public actionSheetCtrl: ActionSheetController,public events: Events, public cadastroService:CadastroServiceProvider,public mensagemUtil:MensagemUtilProvider,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.preencherUsuario();
  }

  public buscarAcoesCamera(){
    let alert = this.alertCtrl.create({
      title: 'Enviar foto',
      message: 'Selecione o local da foto',
      buttons: [
        {
          text: 'Carregar da galeria',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tirar foto',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }

  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      allowEdit: true,
      targetHeight: 100,
      targetWidth: 100
    }
 
    this.camera.getPicture(options)
      .then((imageData) => {
        this.photo = AppServiceProvider.carregarFoto(imageData);
        this.usuario.foto = imageData; 
      }, (error) => {
        console.error(error);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  public preencherUsuario(){
    this.usuario.nome = DadosUsuarioProvider.nome;
    this.usuario.email = DadosUsuarioProvider.email;
    this.usuario.telefone = DadosUsuarioProvider.telefone;
    this.usuario.cep = DadosUsuarioProvider.cep;
    this.usuario.endereco = DadosUsuarioProvider.endereco;
    this.usuario.estado = DadosUsuarioProvider.estado;
    this.usuario.cidade = DadosUsuarioProvider.cidade;
    this.usuario.foto = DadosUsuarioProvider.foto;
    this.photo = AppServiceProvider.carregarFoto(this.usuario.foto);
  }

  atualizar(){
    let loading = this.loadingCtrl.create({
      content: 'Atualizando...'
    });
    loading.present();    
    if(this.usuarioPreenchidoValido(this.usuario)){
      this.cadastroService.atualizar(this.usuario).subscribe(result => {
        DadosUsuarioProvider.atualizarDadosProvider(this.usuario);
        this.events.publish('user:atualizar');
        loading.dismiss();
        this.mensagemUtil.mostrarToastSuccess("Usuário atualizado!");
      }, error => {
        loading.dismiss(),
        this.mensagemUtil.mostrarToastError(error)
      });
    }else{
      loading.dismiss();
    }
  }

  public usuarioPreenchidoValido(usuario:any){
    if(StringUtilProvider.stringValida(usuario.nome) && StringUtilProvider.stringValida(usuario.telefone)){
      if(!StringUtilProvider.stringValida(this.senha) && !StringUtilProvider.stringValida(this.confirmacaoSenha)){
        return true;
      }else{
        if(StringUtilProvider.isSenhaValida(this.senha)){
          if(this.senha == this.confirmacaoSenha){
            return true;
          }else{
            this.mensagemUtil.mostrarToastErrorString("Senha e confirmação de senha devem ser idênticas.");
          }
        }else{
          this.mensagemUtil.mostrarToastErrorString("Senha muito fraca. Senha precisa ter: 8 caracteres e pelo menos 1 número.");
        }
      }
    }else{
      this.mensagemUtil.mostrarToastErrorString("Nome e telefone são obrigatórios.");
    }
    return false;
  }

}
