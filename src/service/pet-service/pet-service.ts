import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiConfig } from '../../constantes/api-config/api-config';

@Injectable()
export class PetServiceProvider {

  constructor(public http: HttpClient) {}

  buscarPetsProximos(lat:number,lng:number): Observable<any> {
    let result: Observable<Object>;
    result = this.http.get(ApiConfig.BUSCAR_PETS_PROXIMOS+'/'+lat+'/'+lng);
    return result.catch(error => Observable.throw(error));
  }

}
