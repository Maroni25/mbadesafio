import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiConfig } from '../../constantes/api-config/api-config';
@Injectable()
export class ConfiguracaoServiceProvider {

  constructor(public http: HttpClient) {}

  salvar(configuracao:any): Observable<any>{
    let header = { headers: new HttpHeaders({ 'Authorization': ApiConfig.tokenAuth})};
    let result: Observable<Object>;  
    result = this.http.post(ApiConfig.SALVAR_CONFIGURACAO,configuracao,header);
    return result.catch(error => Observable.throw(error));
  }  

}
