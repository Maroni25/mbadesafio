import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class MensagemUtilProvider {

  constructor(public toastCtrl:ToastController) {}

  public mostrarToast(mensagem:string){
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'Fechar!',
      dismissOnPageChange: true,
      cssClass: "toast-default"
    });
    toast.present();
  }

  public mostrarToastErrorString(error:string){
    if(error === null || error === undefined || error === ""){
      error = "Erro, tente novamente mais tarde.";
    }
    let toast = this.toastCtrl.create({
      message: error,
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'Fechar!',
      dismissOnPageChange: true,
      cssClass: "toast-error-class",
    });
    toast.present();
  }

  public mostrarToastError(error:any){
    console.log(error);
    let mensagem:string;
    mensagem = "Erro, tente novamente mais tarde.";
    if(error !== null && error !== undefined){
      if(error.error !== null || error.error !== undefined && error.error.mensagem !== null && error.error.mensagem !== undefined){
        mensagem = error.error.mensagem;
      }
    }
    if(mensagem === null || mensagem === undefined || mensagem === ""){
      mensagem = "Erro, tente novamente mais tarde.";
    }
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'Fechar!',
      dismissOnPageChange: true,
      cssClass: "toast-error-class",
    });
    toast.present();
  }

  public mostrarToastSuccess(mensagem:string){
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'Fechar!',
      dismissOnPageChange: true,
      cssClass: "toast-success-class",
    });
    toast.present();
  }
}
