import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AppServiceProvider {

  public static carregarFoto(imageData):string{
    if(imageData !== null && imageData !== undefined){
      return 'data:image/jpeg;base64,' + imageData;
    }
    return '';
  }

}
