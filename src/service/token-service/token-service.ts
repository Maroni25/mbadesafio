import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { HttpClient,HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiConfig } from '../../constantes/api-config/api-config';

@Injectable()
export class TokenServiceProvider {

  constructor(public http: HttpClient, public storage:Storage) {}

  getToken(){
    return this.storage.get('Authorization');
  }

  validarToken(token: String): Observable<any> {
    const headers=new HttpHeaders().set('Authorization', ApiConfig.tokenAuth);   
    let result: Observable<Object>;  
    result = this.http.get(ApiConfig.VALIDAR_TOKEN_API,{headers,observe: 'response'});
    return result.catch(error => Observable.throw(error));
  }
}
