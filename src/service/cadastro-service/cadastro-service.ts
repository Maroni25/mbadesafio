import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApiConfig } from '../../constantes/api-config/api-config';

@Injectable()
export class CadastroServiceProvider {

  constructor(public http: HttpClient) {}

  logar(usuario: any): Observable<any>{    
    let result: Observable<Object>;  
    result = this.http.post(ApiConfig.LOGIN_API,usuario,{observe: 'response'});
    return result.catch(error => Observable.throw(error));
  }

  desconectar(): Observable<any>{    
    const headers=new HttpHeaders().set('Authorization', ApiConfig.tokenAuth);   
    let result: Observable<Object>;  
    result = this.http.get(ApiConfig.DESCONECTAR_USUARIO,{headers,observe: 'response'});
    return result.catch(error => Observable.throw(error));
  }

  recuperarCodigo(email:String): Observable<any> {
    let user = {email:email}
    let result: Observable<Object>;
    result = this.http.post(ApiConfig.RECUPERAR_CODIGO_EMAIL, user)
    return result.catch(error => Observable.throw(error));
  }

  mudarSenha(usuario: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(ApiConfig.ALTERAR_SENHA, usuario)
    return result.catch(error => Observable.throw(error));
  }

  cadastrar(usuario: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(ApiConfig.CADASTRAR_API, usuario)
    return result.catch(error => Observable.throw(error));
  }

  atualizar(usuario:any): Observable<any>{
    let header = { headers: new HttpHeaders({ 'Authorization': ApiConfig.tokenAuth})};
    let result: Observable<Object>;  
    result = this.http.post(ApiConfig.ATUALIZAR_USUARIO,usuario,header);
    return result.catch(error => Observable.throw(error));
  }  
}
