import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { RastreioDonoPage } from '../pages/rastreio-dono/rastreio-dono';
import { MeuBichoPage } from '../pages/meu-bicho/meu-bicho';
import { MensagemPage } from '../pages/mensagem/mensagem';
import { SobrePage } from '../pages/sobre/sobre';
import { PerfilPage } from '../pages/perfil/perfil';
import { ConfiguracaoPage } from '../pages/configuracao/configuracao';
import { LoginPage } from '../pages/login/login';
import { TokenServiceProvider } from '../service/token-service/token-service';
import { Storage } from '@ionic/storage';
import { ApiConfig } from '../constantes/api-config/api-config';
import { CadastroServiceProvider } from '../service/cadastro-service/cadastro-service';
import { MensagemUtilProvider } from '../service/mensagem-util/mensagem-util';
import { DadosUsuarioProvider } from '../providers/dados-usuario/dados-usuario';
import { AppServiceProvider } from '../service/app-service/app-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{title: string, component: any, icone: string}>;
  public usuarioLogado:boolean = false;
  public nome:string;
  public foto:string;

  constructor(private mensagemUtil:MensagemUtilProvider,private cadastroService:CadastroServiceProvider,public menuCtrl: MenuController, public events: Events, private storage: Storage,public tokenService:TokenServiceProvider,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    events.subscribe('user:atualizar', () => {
      this.atualizarDadosUsuario();
    });
    events.subscribe('user:login', () => {
      this.carregarPaginaUsuarioLogado();
    });
    events.subscribe('user:entrarSemLogin', () => {
      this.carregarPaginaUsuarioDeslogado();
    });
    events.subscribe('user:carregarPaginaInicial', () => {
      this.carregarPaginaInicial();
    });
    this.initializeApp();
  }

  atualizarDadosUsuario(){
    this.nome = DadosUsuarioProvider.nome;
    this.foto = AppServiceProvider.carregarFoto(DadosUsuarioProvider.foto); 
  }

  carregarPaginaInicial(){
    if(ApiConfig.tokenAuth){
      this.sair();
    }
    this.storage.set('Authorization',null);
    this.menuCtrl.enable(false, 'myMenu');
    ApiConfig.tokenAuth = null;
    this.rootPage = LoginPage;
  }

  public sair(){
    this.cadastroService.desconectar().subscribe(result => {
    }, error =>  this.mensagemUtil.mostrarToast(error))
  }

  validarToken(){
    this.tokenService.getToken().then((token) => {
      if(token !== null && token !== undefined){
        ApiConfig.tokenAuth = token;
        this.tokenService.validarToken(token).subscribe(result => {
          DadosUsuarioProvider.preencherDadosProvider(result.headers.get("dados-usuario"));
          this.nome = DadosUsuarioProvider.nome;
          this.foto = DadosUsuarioProvider.foto;
          this.carregarPaginaUsuarioLogado();
        }, error => {
          this.carregarPaginaInicial();
        })
      }else{
        this.carregarPaginaInicial();
      }
    });
  }

  carregarPaginaUsuarioLogado(){
    this.atualizarDadosUsuario();
    this.usuarioLogado = true;
    this.rootPage = HomePage;
    this.preencherMenu();
  }

  carregarPaginaUsuarioDeslogado(){
    this.usuarioLogado = false;
    this.rootPage = HomePage;
    this.preencherMenu();
  }

  public abrirPerfil(){
    this.nav.setRoot(PerfilPage);
  }

  public abrirConfig(){
    this.nav.setRoot(ConfiguracaoPage);
  }

  public preencherMenu(){
    this.menuCtrl.enable(true, 'myMenu');
    if(this.usuarioLogado){
      this.preencherMenuLogado();
    }else{
      this.preencherMenuDeslogado();
    }
  }

  public preencherMenuLogado(){
    if(this.usuarioLogado){
      this.pages = [
        { title: 'Perdidos', component: HomePage,icone:'map'},
        //{ title: 'Procurar dono', component: RastreioDonoPage,icone:'search'},
        { title: 'Meus bichos', component: MeuBichoPage,icone:'heart'},
        { title: 'Mensagens', component: MensagemPage,icone:'text'},
        //{ title: 'Compartilhar', component: HomePage,icone:'share'},
        //{ title: 'Sobre', component: SobrePage,icone:'paper'},
        { title: 'Deslogar', component: LoginPage,icone:'exit'}
      ];
    }
  }

  public preencherMenuDeslogado(){
    if(!this.usuarioLogado){
      this.pages = [
        { title: 'Perdidos', component: HomePage,icone:'search'},
        //{ title: 'Procurar dono', component: RastreioDonoPage,icone:'search'},
       // { title: 'Compartilhar', component: HomePage,icone:'share'},
        //{ title: 'Sobre', component: SobrePage,icone:'paper'},
        { title: 'Cadastrar', component: LoginPage,icone:'create'},
        { title: 'Logar', component: LoginPage,icone:'exit'}
      ];
    }
  }

  initializeApp() {
    this.validarToken();
    this.platform.ready().then(() => {});
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
