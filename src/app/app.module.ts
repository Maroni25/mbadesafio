import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AnuncioPage } from '../pages/anuncio/anuncio';
import { ConfiguracaoPage } from '../pages/configuracao/configuracao';
import { MensagemPage } from '../pages/mensagem/mensagem';
import { MeuBichoPage } from '../pages/meu-bicho/meu-bicho';
import { RastreioDonoPage } from '../pages/rastreio-dono/rastreio-dono';
import { SobrePage } from '../pages/sobre/sobre';
import { PerfilPage } from '../pages/perfil/perfil';
import { LoginPage } from '../pages/login/login';
import { IonicStorageModule } from '@ionic/storage';
import { TokenServiceProvider } from '../service/token-service/token-service';
import { HttpClientModule } from '@angular/common/http';
import { CadastroServiceProvider } from '../service/cadastro-service/cadastro-service';
import { MensagemUtilProvider } from '../service/mensagem-util/mensagem-util';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { PetServiceProvider } from '../service/pet-service/pet-service';
import { CadastroPetPage } from '../pages/cadastro-pet/cadastro-pet';
import { DadosUsuarioProvider } from '../providers/dados-usuario/dados-usuario';
import { ConfiguracaoServiceProvider } from '../service/configuracao-service/configuracao-service';
import { StringUtilProvider } from '../providers/string-util/string-util';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { GoogleMaps } from '@ionic-native/google-maps';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AnuncioPage,
    ConfiguracaoPage,
    MensagemPage,
    MeuBichoPage,
    RastreioDonoPage,
    SobrePage,
    PerfilPage,
    LoginPage,
    CadastroPetPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AnuncioPage,
    ConfiguracaoPage,
    MensagemPage,
    MeuBichoPage,
    RastreioDonoPage,
    SobrePage,
    PerfilPage,
    LoginPage,
    CadastroPetPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TokenServiceProvider,
    CadastroServiceProvider,
    MensagemUtilProvider,
    NativeGeocoder,
    Geolocation,
    PetServiceProvider,
    DadosUsuarioProvider,
    ConfiguracaoServiceProvider,
    StringUtilProvider,
    File,
    Transfer,
    Camera,
    FilePath,
    GoogleMaps
  ]
})
export class AppModule {}
