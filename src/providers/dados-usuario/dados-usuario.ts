import { Injectable } from '@angular/core';

interface DadosUsuario {
  nome:string;
  email:string;
  telefone:string;
  foto:string;
  configuracao:any;
  animais:any;
}

@Injectable()
export class DadosUsuarioProvider {

  public static nome:string;
  public static email:string;
  public static telefone:string;
  public static foto:string;
  public static configuracao:any;
  public static animais:any;
  public static cep:string;
  public static endereco:string;
  public static cidade:string;
  public static estado:string;

  public static atualizarDadosProvider(dadosUsuario:any){
    DadosUsuarioProvider.nome = dadosUsuario.nome;
    DadosUsuarioProvider.email = dadosUsuario.email;
    DadosUsuarioProvider.telefone = dadosUsuario.telefone;
    DadosUsuarioProvider.foto = dadosUsuario.foto;
    DadosUsuarioProvider.configuracao = dadosUsuario.configuracao;
    DadosUsuarioProvider.animais = dadosUsuario.animais;
  }

  public static preencherDadosProvider(dadosUsuarioBase:any){
    let dadosUsuario:DadosUsuario = JSON.parse(dadosUsuarioBase);
    DadosUsuarioProvider.nome = dadosUsuario.nome;
    DadosUsuarioProvider.email = dadosUsuario.email;
    DadosUsuarioProvider.telefone = dadosUsuario.telefone;
    DadosUsuarioProvider.foto = dadosUsuario.foto;
    DadosUsuarioProvider.configuracao = dadosUsuario.configuracao;
    DadosUsuarioProvider.animais = dadosUsuario.animais;
  }

}
