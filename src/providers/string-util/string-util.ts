import { Injectable } from '@angular/core';

@Injectable()
export class StringUtilProvider {

  constructor() {}

  public static stringValida(texto:string):boolean{
    if(texto !== null && texto !== undefined && texto !== ''){
      return true;
    }
    return false;
  }

  public static isSenhaValida(senha:string):boolean{
    var regexp = new RegExp("^(?=.*\\d).{8,}$"),
    validacao = regexp.test(senha);
    return validacao;
  }

}
