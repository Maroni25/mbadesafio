import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiConfig {
  //public static API = 'http://localhost:8080';
  public static API = 'https://petperdido.herokuapp.com';
  public static CADASTRAR_API = ApiConfig.API + '/public/cadastrar';
  public static RECUPERAR_CODIGO_EMAIL = ApiConfig.API + '/public/recuperar/codigo';
  public static ALTERAR_SENHA = ApiConfig.API + '/public/alterar/senha';
  public static LOGIN_API = ApiConfig.API + '/public/autenticar';
  public static VALIDAR_TOKEN_API = ApiConfig.API + '/private/validarToken';
  public static DESCONECTAR_USUARIO = ApiConfig.API + '/private/logout';
  public static BUSCAR_PETS_PROXIMOS = ApiConfig.API + '/public/petsperdidos';
  public static SALVAR_CONFIGURACAO = ApiConfig.API + '/private/configuracao';
  public static ATUALIZAR_USUARIO = ApiConfig.API + '/private/usuario';
  public static tokenAuth:string;
  
  constructor() {}

}
